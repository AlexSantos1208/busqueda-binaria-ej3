package ejercicio.pkg3;

import java.util.Scanner;

public class Busqueda_binaria {

    
    public static void main(String[] args) {
        //arreglos
        int[] num_factura = new int[5];
        String[] nom_cliente = new String[5];
        String[] fecha_facturacion = new String[5];
        double[] valor_factura = new double[5];
        
        //variables auxiliares 
        int inicio = 0;
        int fin = num_factura.length -1;
        
        //contenido de arreglos
        num_factura[0] = 11045;
        num_factura[1] = 11071;
        num_factura[2] = 11020;
        num_factura[3] = 11095;
        num_factura[4] = 11037;
        
        nom_cliente[0] = "Alexander Ibrahim De La Cruz Santos";
        nom_cliente[1] = "Cristian Berzunza Poot";  
        nom_cliente[2] = "William Manuel Vargas Cetz";
        nom_cliente[3] = "Juan Carlos Avilez Miss";
        nom_cliente[4] = "Dalia Padilla Paz";
        
        fecha_facturacion[0] = "3/10/2021";
        fecha_facturacion[1] = "4/10/2021";
        fecha_facturacion[2] = "5/10/2021";
        fecha_facturacion[3] = "6/10/2021";
        fecha_facturacion[4] = "7/10/2021";
        
        valor_factura[0] = 250.50;
        valor_factura[1] = 60.10;
        valor_factura[2] = 250.70;
        valor_factura[3] = 960.00;
        valor_factura[4] = 1000.50;
        
        Scanner sc = new Scanner(System.in);
        System.out.println("-----------Almacen Todocaro-----------");
        
        //ordenamos el arreglo
        ordenarArr(num_factura, inicio, fin);
        
        //imprimimos el arreglo
        imprimirArreglo(num_factura);
        
        //solicitamos que digite la factura a buscar 
        System.out.print("\n\nDigite la factura que desea buscar: ");
        int dato = sc.nextInt();
        
        //buscamos el dato
        busq_binaria(num_factura, dato);
        
        //variable auxiliar
        int posicion = busq_binaria(num_factura, dato);
        if(posicion == -1){
            
        }
        else{
            imprimirDatos(num_factura,nom_cliente, fecha_facturacion, valor_factura, posicion);
        }
        
        
    }
    public static void imprimirArreglo(int[] arr){
        System.out.println("\n      ----------Facturas-----------\n");
        for(int i = 0; i<arr.length; i++){           
            System.out.print("["+arr[i]+"] ");
        }
        
    }
    //metodo quicksort
    public static void ordenarArr(int[] arr, int inicio, int fin){
                if(inicio>=fin) return;
                int pivote=arr[inicio];
                int elemIzq=inicio+1;
                int elemDer=fin;
                while(elemIzq<=elemDer){
                        while(elemIzq<=fin && arr[elemIzq]<pivote){
                                elemIzq++;
                        }
                        while(elemDer>inicio && arr[elemDer]>=pivote){
                                elemDer--;
                        }
                        if(elemIzq<elemDer){
                                int temp=arr[elemIzq];
                                arr[elemIzq]=arr[elemDer];
                                arr[elemDer]=temp;
                        }
                }
                
                if(elemDer>inicio){
                        int temp=arr[inicio];
                        arr[inicio]=arr[elemDer];
                        arr[elemDer]=temp;
                }
                ordenarArr(arr, inicio, elemDer-1);
                ordenarArr(arr, elemDer+1, fin);
        }
           
    public static int busq_binaria(int[] arr,int dato){
     int posicion = 0;
     int min = 0;
     int max = arr.length -1;
     int i = 0;
     boolean enc = false;
            
     while(min <= max && i<arr.length){
        
         
        double mitad = (double)(min+max)/2;
        mitad = Math.round(mitad);
        int media = (int)mitad;
        
         if(arr[media] == dato){
             enc = true;
             posicion = media;
             break;     
        }
         if(arr[media] < dato){
            min = media + 1;
        }
        if(arr[media] > dato){
            max = media - 1;      
        }
         i++;
     }
    
     if (enc == true){
         
     }
     else{
         System.out.println("Dato Incorrecto");
         posicion = -1;
     }
     return posicion;
    }
    
    public static void imprimirDatos(int[] num_factura,String[] nomb_cliente, String[] fecha_facturacion, double[] valor_factura, int posicion){
        System.out.println("\n    Datos de la Factura No.["+num_factura[posicion]+"]\n");
        System.out.println("Nombre del cliente:      "+nomb_cliente[posicion]);
        System.out.println("Fecha de facturacion:    "+fecha_facturacion[posicion]);
        System.out.println("Valor de la facutra:     "+valor_factura[posicion]);
        System.out.println("---------------------------------------");
    }
    
}
